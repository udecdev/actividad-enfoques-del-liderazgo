window.onload = function() {
    // Código a ejecutar cuando se ha cargado toda la página
 
   // stage2.stop();
    main();
};
var audios = [{
    url: "assets/sounds/click.mp3",
    name: "clic"
}];
ivo.info({
    title: "Universidad de Cindinamarca",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
questions=[
    {
        image:"./assets/img/ico-4.png",
        question:"Haciendo un autodiagnóstico, usted se da cuenta que como líder tiene toda la preparación intelectual para afrontar el liderazgo en su área de trabajo. Usted considera que nació para liderar y sus colaboradores nacieron únicamente para obedecer sus órdenes. De este modo, usted se identifica con el siguiente enfoque del liderazgo:",
        options:[
            {
                option:"Prospectivo",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque prospectivo es visionario por naturaleza y no se basa en las capacidades del líder." 
            },{
                option:"Situacional",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque situacional asume que el liderazgo depende de cada situación en particular , pero no está fundamentado en las capacidades del líder." 
            },{
                option:"Personalista",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque personalista se basa en el carisma personal, pero no en las capacidades desarrolladas por el líder." 
            },{
                option:"Sustancialista",
                correct:true,
                retro:"<span>Correcto:</span> El enfoque sustancialista se basa en las capacidades y habilidades que desarrolla el líder y que lo distinguen de los demás miembros de la organización, ya que lo dota de las facultades suficientes para ejercer su rol de liderazgo" 
            }       
        ]
    },
    {
        image:"./assets/img/ico-5.png",
        question:"Usted en su rol de liderazgo decide adoptar un enfoque basado en el comportamiento, y se da cuenta que puede aplicar su propio grid gerencial, para identificar su estilo propio. Haciendo un autoanálisis, se da cuenta que su interés por los colaboradores a su cargo es promedio. Así como, la exigencia en el cumplimiento de las tareas es moderada. De este modo, su estilo gerencial está basado en:",
        options:[
            {
                option:"Administración de hombre-organización",
                correct:true,
                retro:"<span>Correcto:</span> El estilo de administración hombre-organización es aquel que consiste en un interés medio por las personas, así como, un interés promedio por las tareas. De este modo, es el estilo gerencial caracterizado en el enunciado de la pregunta" 
            },{
                option:"Administración del equipo",
                correct:false,
                retro:"<span>Incorrecto:</span> El estilo de administración del equipo se basa en un interés alto tanto por las personas como por las tareas. Por lo tanto, no es coherente con el estilo descrito en el enunciado" 
            },{
                option:"Personalista",
                correct:false,
                retro:"<span>Incorrecto:</span> El estilo de administración pobre se basa en un interés bajo tanto por las personas como por las tareas. Por lo tanto, no es coherente con el estilo descrito en el enunciado." 
            },{
                option:"Sustancialista",
                correct:false,
                retro:"<span>Incorrecto:</span> El estilo de autoridad-obediencia se basa en un interés bajo tanto por las personas pero un interés alto por las tareas. Por lo tanto, no es coherente con el estilo descrito en el enunciado" 
            }       
        ]
    },
    {
        image:"./assets/img/ico-6.png",
        question:"En su área de trabajo, usted detecta que uno de sus colaboradores requiere de un refuerzo positivo para la mejora continua de su desempeño. Otro de sus colaboradores requiere de un refuerzo negativo frente al desarrollo de sus actividades y compromisos. De este modo, el enfoque de liderazgo que usted puede aplicar en este caso es:",
        options:[
            {
                option:"Enfoque comportamental",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque comportamental se basa en las actitudes que adopta el líder en términos del interés por las personas y las tareas. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            },{
                option:"Enfoque situacional",
                correct:true,
                retro:"<span>Correcto:</span> El enfoque situacional de liderazgo se basa en la premisa de que el estilo de liderazgo se adapta de acuerdo a cada circunstancia o persona en particular. Por lo tanto, es coherente con la situación que se establece en el enunciado de la pregunta" 
            },{
                option:"Enfoque personalista",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque personalista se basa en el carisma personal innato en la esencia del líder. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            },{
                option:"Enfoque sustancialista",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque sustancialista se basa en las capacidades y habilidades que desarrolla el líder y que lo distinguen de los demás miembros de la organización. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            }       
        ]
    },
    {
        image:"./assets/img/ico-7.png",
        question:"Usted se percata, de que su esencia desde siempre ha sido liderar, guiar o inspirar a otros a alcanzar los objetivos propuestos, así como, autor reflexiona constantemente sobre su estilo innato de liderazgo para mejorarlo continuamente. De este modo y según el enunciado, usted se identificaría con el siguiente enfoque del liderazgo:",
        options:[
            {
                option:"Enfoque prospectivo",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque prospectivo se basa en una visión de futuro para encaminar los esfuerzos de los miembros de la organización. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            },{
                option:"Enfoque comportamental",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque comportamental se basa en las actitudes que adopta el líder en términos del interés por las personas y las tareas. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            },{
                option:"Enfoque personalista",
                correct:true,
                retro:"<span>Correcto:</span> Desde el concepto del enfoque personalista se analiza que el liderazgo depende fundamentalmente de las condiciones naturales y esenciales del líder, así como, se propende por el autofortalecimiento del mismo , involucrando la ética y el comportamiento. Por lo tanto, este estilo es perfectamente coherente con el enunciado propuesto." 
            },{
                option:"Enfoque situacional",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque situacional se basa en la premisa de que el estilo de liderazgo se adapta de acuerdo a cada circunstancia o persona en particular. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            }       
        ]
    },
    {
        image:"./assets/img/ico-8.png",
        question:"En su área de trabajo, usted se da cuenta que puede plantear un escenario apuesta a futuro que permita encaminar los esfuerzos de sus colaboradores a un alto desempeño. De esta manera, articula una visión de futuro y unas estrategias que permiten materializar en la realidad este ideal. Por lo tanto, el enfoque del liderazgo que mejor aplica en este enunciado corresponde a:",
        options:[
            {
                option:"Enfoque prospectivo",
                correct:true,
                retro:"<span>Correcto:</span> El enfoque prospectivo se basa en una visión de futuro para encaminar los esfuerzos de los miembros de la organización. Por lo tanto, este enfoque de liderazgo es el que mejor se ajusta al enunciado descrito." 
            },{
                option:"Enfoque situacional",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque situacional se basa en la premisa de que el estilo de liderazgo se adapta de acuerdo a cada circunstancia o persona en particular. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            },{
                option:"Enfoque comportamental",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque comportamental se basa en las actitudes que adopta el líder en términos del interés por las personas y las tareas. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            },{
                option:"Enfoque personalista",
                correct:false,
                retro:"<span>Incorrecto:</span> El enfoque personalista se basa en el carisma personal innato en la esencia del líder. Por lo tanto, este enfoque no se ajusta a la descripción del enunciado" 
            }       
        ]
    }
];
var question = questions.length;
var points = 0;
function main() {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.generateQuestions();
            t.events();
            t.animations();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo("#preload").hide();
                stage1.play();
            });
        },
        methods: {
            generateQuestions: function () {
                for (let index1 = 0; index1 < questions.length; index1++) {
                    const element = questions[index1];
                    const indice = index1 + 1;
                    var html = `
                    <div class="stage slides" id="slider${indice}">
                        <div class="stage_static d-flex question">
                            <div class="not-movil">
                                <img src="${element.image}" alt="${element.question}">
                            </div>
                            <div>
                                ${element.question}
                            </div>
                        </div>
                        <ul  class="stage_static options">
                            {{quetions}}
                        </ul>
                        
                    </div>
                    `;
                    var quetions = "";
                    var abc = ["A", "B", "C", "D"];
                    for (let index2 = 0; index2 < element.options.length; index2++) {
                        option = element.options[index2];
                        quetions += `
                        <li class="options" id="r${index1}-${index2}" data-selected="false" data-value="${option.correct}" >
                            <div class="num"><p>${abc[index2]}</p></div>
                            <span>${option.option}</span> 
                            <div class="retro"> 
                                ${option.retro}
                            </div>
                        </li>
                        `;
                    }
                    html = html.replace("{{quetions}}", quetions);
                    ivo("#slider").append(html);  
                }
                
                
                ivo(".options").on("click", function () {
                    if(ivo(this).attr("data-selected")=="false"){
                        question--;
                        ivo(this).attr("data-selected", "true");
                        //quito el evento a los li hermanos con js puro
                        let correct = ivo(this).attr("data-value");
                        let id = "#"+ivo(this).attr("id");
                        //bucamos a los li hermanos 
                        let hermanos = ivo(this).parent().children();
                        hermanos = hermanos.elements;
                        for (let index = 0; index < hermanos.length; index++) {
                            const element = hermanos[index];
                            ivo(element).attr("data-selected", "true");
                        }
                        if (correct=="true") {
                            ivo(id + " .retro").show();
                            ivo(id + " .num p").css("background", "#79BF00");
                            points=points+1;
                        } else {
                            ivo(id + " .retro").show();
                            ivo.play("clic");
                        }
                        if (question==0) {
                            //traformamos poits en una escala de 50
                            let porcentaje = (points*50)/questions.length;
                            console.log(porcentaje);
                            var scorm= new UdeCScorm(false);
                            console.log("Nombre: " + scorm.info_user().name + " Id: " + scorm.info_user().id + " Nota: " + porcentaje);
                            scorm.set_score(porcentaje);
                            if (porcentaje>30){
                                swal({
                                    title: "Buen trabajo!",
                                    text: "Has obtenido un "+porcentaje+"/50 de aciertos",
                                    icon: "success",
                                    button: "OK",
                                });
                            }else{
                                swal({
                                    title: "Lo sentimos!",
                                    text: "Has obtenido un "+porcentaje+"/50 de aciertos",
                                    icon: "error",
                                    button: "OK",
                                });
                            }
                            

                        }
                    }
                });
            },
            events: function () {
                ivo("#btn-start").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage2").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#btn_start").on("click", function () {
                    stage3.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });
                
                var slider=ivo("#slider").slider({
                    slides:'.slides',
                    btn_next:"#btn_next",
                    btn_back:"#btn_prev",
                    rules:function(rule){
                        console.log("rules"+rule);
                       
                        
                    },
                    onMove:function(page){
                        console.log("onMove"+page);
                        ivo.play("clic");
                      
                    },
                    onFinish:function(){
                    }
                });
            },
            animations: function () {
                stage1 = new TimelineMax();
                
                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".triangulos", .8, {y: 1300,x:400, opacity: 0}, .2), 0);
                stage1.append(TweenMax.from("#stage1_mensage", .8, {x: 1300, opacity: 0}), 0);

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from("#stage2", .8, {y: 1300, opacity: 0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.from("#stage3", .8, {y: 1300, opacity: 0}), 0);
                stage3.stop();

                let movil = false;
                let windowWidth = window.innerWidth;
                if (windowWidth < 1024) {
                    movil = true;
                    
                }
                if('ontouchstart' in window || navigator.maxTouchPoints) {
                    movil = true;
                }
                if (movil) {
                    stage2.play();
                    stage3.play();
                }

            }
        }
    });
}