class UdeCScorm {
    constructor(debug) {
        this.debugger = debug, 0 == this.debugger && (this.scorm = pipwerks.SCORM, this.scorm.version = "1.2", console.log("Inicialización."), this.callSucceeded = this.scorm.init())
    }
  
    
    complete() {
        this.callSucceeded = this.scorm.set("cmi.core.lesson_status", "completed");
    }
    incomplete() {
        this.callSucceeded = this.scorm.set("cmi.core.lesson_status", "incomplete");
    }
    end() {
        var callSucceeded;
        return this.scorm.quit()
    }
    get(item) {
        this.scorm.get(item)
    }
    set(item, save) {
        this.scorm.set(item, save)
    }
    info_user() {
        var dates;
        return {
            name: 0 == this.debugger ? String(this.scorm.get("cmi.core.student_name")) : "Test User",
            id: 0 == this.debugger ? String(this.scorm.get("cmi.core.student_id")) : "Test identification"
        }
    }
    reset_commit() {
        this.set_commit("cmi.core.lesson_location", null)
    }
    set_time(save) {
        0 == this.debugger ? this.scorm.set("cmi.core.session_time", save) : localStorage.setItem("cmi.core.session_time", save)
    }
    set_location(save) {
        0 == this.debugger ? this.scorm.set("cmi.core.lesson_location", save) : localStorage.setItem("cmi.core.lesson_location", save)
    }
    get_location() {
        var commit = 0 == this.debugger ? String(this.scorm.get("cmi.core.lesson_location")) : String(localStorage.getItem("cmi.core.lesson_location"));
        return "" == commit || null == commit ? null : JSON.parse(commit)
    }
    set_entry(save) {
        0 == this.debugger ? this.scorm.set("cmi.interactions.0.time", save) : localStorage.setItem("cmi.interactions.0.time", save)
    }
    set_finish(save) {
        0 == this.debugger ? this.scorm.set("cmi.interactions.1.time", save) : localStorage.setItem("cmi.interactions.1.time", save)
    }
    set_commit(save) {
        0 == this.debugger ? (save = JSON.stringify(save), this.scorm.set("cmi.comments", save)) : (save = JSON.stringify(save), localStorage.setItem("cmi.comments", save))
    }
    get_commit() {
        var commit = 0 == this.debugger ? String(this.scorm.get("cmi.comments")) : String(localStorage.getItem("cmi.comments"));
        return "" == commit || null == commit ? null : commit
    }
    set_score(note) {
        0 == this.debugger ? (this.set("cmi.core.score.raw", String(note)), this.set("Write", String(note / 100)), this.complete(), this.end()) : (localStorage.setItem("cmi.score.raw", String(note)), localStorage.setItem("Write", String(note / 100)))
    }
    get_score(nota) {
        if ("raw" == nota) return 0 == this.debugger ? this.scorm.get("cmi.core.score.raw") : localStorage.getItem("cmi.core.score.raw")
    }
}